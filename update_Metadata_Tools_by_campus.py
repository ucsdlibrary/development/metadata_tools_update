import json
import sys

'''
Set campus_choice to one of the following values:
    * Berkeley
    * Davis
    * Irvine
    * Los Angeles
    * Merced
    * Riverside
    * San Diego
    * San Francisco
    * Santa Barbara
    * Santa Cruz
'''
campus_choice = 'Santa Cruz1' # <-- This value needs to be set

# Confirm existence of the campuses.json file or exit
try:
    with open('campuses.json', 'r') as f:
        campuses_dict = json.load(f)
        campuses = enumerate(campuses_dict.keys(), start=1)
except FileNotFoundError as e:
    print(e)
    sys.exit('Exiting...')

# Confirm valid campus entry
try:
    selected_campus = campuses_dict[campus_choice]
except KeyError:
    print(f'\nUnable to find selected value: {campus_choice}')
    print(f'Valid Campus values:')
    for key in sorted(campuses_dict.keys()):
        print(f'\t{key}')
    sys.exit('\nExiting...')


with open('Metadata_Tools.pyt', 'r', encoding='utf-8') as file, open(f'{selected_campus["short_campus"]}_Metadata_Tools.pyt', 'w') as outfile:
    contents = file.readlines()
    for line in contents:
        org_line = line
        if 'UC Santa Barbara' in line:
            line = line.replace('UC Santa Barbara', f'{selected_campus["mid_campus"]}')
            print(f'\n{org_line.strip()}')
            print(f'{line.strip()}')
            outfile.write(line)
        elif 'Santa Barbara' in line:
            if line.strip().startswith('orgName'):
                line = line.replace('Santa Barbara', f'{selected_campus["name"]}')
            elif line.strip().startswith('cityText'):
                line = line.replace('Santa Barbara', f'{selected_campus["City"]}')
            print(f'\n{org_line.strip()}')
            print(f'{line.strip()}')
            outfile.write(line)
        elif 'UCSB' in line:
            if line.strip().startswith('descriptionText2'):
                line = line.replace('UCSB', f'{selected_campus["short_campus"]}').replace('obtained [date] ', 'obtained ')
            else:
                line = line.replace('UCSB', f'{selected_campus["short_campus"]}')
            print(f'\n{org_line.strip()}')
            print(f'{line.strip()}')
            outfile.write(line)
        elif line.strip().startswith('postCodeText'):
            line = line.replace('93106-9010', f'{selected_campus["zip"]}')
            print(f'\n{org_line.strip()}')
            print(f'{line.strip()}')
            outfile.write(line)
        else:
            outfile.write(line)
